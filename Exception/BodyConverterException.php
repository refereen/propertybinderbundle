<?php
/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */
declare(strict_types=1);
namespace LP\PropertyBinderBundle\Exception;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class InvalidDataException
 * @package LP\PropertyBinderBundle\Exception
 */
class BodyConverterException extends Exception {
    /**
     * @var ConstraintViolationListInterface
     */
    private $constraintViolationList;

    /**
     * InvalidDataException constructor.
     * @param ConstraintViolationListInterface $constraintViolationList
     * @param string                           $message
     * @param int                              $code
     * @param Exception|null                   $previous
     */
    public function __construct(ConstraintViolationListInterface $constraintViolationList, $message = 'Validation Error', $code = 400, Exception $previous = null) {

        $this->constraintViolationList = $constraintViolationList;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getConstraintViolationList():ConstraintViolationListInterface {
        return $this->constraintViolationList;
    }
}