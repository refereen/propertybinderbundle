<?php
/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */
namespace LP\PropertyBinderBundle\DependencyInjection\Compiler;

use LP\PropertyBinder\Handler\PropertyBindingRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class PropertyBindingCompilerPass
 * @package LP\PropertyBinderBundle\DependencyInjection\Compiler
 */
class PropertyBindingCompilerPass implements CompilerPassInterface {

    const SERVICE = 'lp_property_binder.binding_registry';
    const TAG_NAME = 'lp_property_binder.binding';
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container) {
        if(!$container->hasDefinition(self::SERVICE)) {
            return;
        }

        $propertyBinderRegistryDefinition = $container->findDefinition(self::SERVICE);
        $taggedServices = $container->findTaggedServiceIds(self::TAG_NAME);

        foreach($taggedServices as $id => $tags) {
            $propertyBinderRegistryDefinition->addMethodCall('register', array(new Reference($id)));
        }
        $propertyBinderRegistryDefinition->addMethodCall('sortBindings');
    }
}