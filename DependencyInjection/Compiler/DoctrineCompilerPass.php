<?php
/**
 * Copyright (c) 2019. Wortzwei GmbH rene@moellmer.net
 */
declare(strict_types=1);
namespace LP\PropertyBinderBundle\DependencyInjection\Compiler;

use LP\PropertyBinder\Doctrine\DoctrineDocumentReference;
use LP\PropertyBinder\Doctrine\DoctrineEntityReference;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class DoctrineCompilerPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container) {

        $manager = $container->getParameter('lp_property_binder.manager');
        if(empty($manager)) return;


        $documentReferenceDefinition = null;
        if($manager === 'document') {
            $mongoDocumentServiceId = null;
            if($container->hasDefinition('doctrine.odm.mongodb.document_manager')) {
                $mongoDocumentServiceId = 'doctrine.odm.mongodb.document_manager';
            } else if($container->hasDefinition('doctrine_mongodb.odm.default_document_manager')) {
                $mongoDocumentServiceId = 'doctrine_mongodb.odm.default_document_manager';
            } else {
                throw new \RuntimeException(sprintf('You have add a document manager but none where found. Did you installed the mongo odm bundle?'));
            }
            $documentReferenceDefinition = new Definition(DoctrineDocumentReference::class);
            $documentReferenceDefinition
                ->setPublic(false)
                ->addArgument(new Reference($mongoDocumentServiceId));
        } else {
            $documentReferenceDefinition = new Definition(DoctrineEntityReference::class);
            $documentReferenceDefinition
                ->setPublic(false)
                ->addArgument(new Reference('doctrine.orm.entity_manager'));
        }
        $container
            ->setDefinition('lp_property_binder.doctrine_reference', $documentReferenceDefinition);

        $objectBindingDefinition = $container->getDefinition('lp_property_binder.binding.object_binding');
        $objectBindingDefinition->replaceArgument(0, new Reference('lp_property_binder.doctrine_reference'));

        $objectCollectionBindingDefinition = $container->getDefinition('lp_property_binder.binding.object_collection_binding');
        $objectCollectionBindingDefinition->replaceArgument(0, new Reference('lp_property_binder.doctrine_reference'));
    }
}