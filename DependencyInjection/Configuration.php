<?php
/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */
namespace LP\PropertyBinderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

    const ROOT_NODE = 'lp_property_binder';

    /**
     * @var bool
     */
    private $debug;

    /**
     * @param bool $debug
     */
    public function __construct(bool $debug) {
        $this->debug = $debug;
    }

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        if (method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode()->children();
        } else {
            $root = $treeBuilder->root(self::ROOT_NODE)->children();
        }

        $this->addMetadataNode($root);
        $this->addDoctrineNode($root);

        return $treeBuilder;
    }

    private function addMetadataNode(NodeBuilder $builder) {
        $builder
            ->arrayNode('metadata')
                ->addDefaultsIfNotSet()
                ->fixXmlConfig('directory', 'directories')
                ->children()
                    ->scalarNode('cache')->defaultValue('file')->end()
                    ->booleanNode('debug')->defaultValue($this->debug)->end()
                    ->arrayNode('file_cache')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('dir')->defaultValue('%kernel.cache_dir%/property_binder')->end()
                        ->end()
                    ->end()
                    ->booleanNode('auto_detection')->defaultFalse()->end()

                    ->arrayNode('directories')
                        ->useAttributeAsKey('name')
                        ->prototype('array')
                        ->children()
                            ->scalarNode('path')->isRequired()->end()
                            ->scalarNode('namespace_prefix')->defaultValue('')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    private function addDoctrineNode(NodeBuilder $builder) {
        $builder
            ->arrayNode('doctrine')
                ->addDefaultsIfNotSet()
                ->children()
                    ->enumNode('manager')->values(['entity', 'document'])->defaultValue('document')
                ->end()
            ->end();
        return $builder;
    }
}