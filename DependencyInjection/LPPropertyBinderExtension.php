<?php
/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */
namespace LP\PropertyBinderBundle\DependencyInjection;

use LP\PropertyBinder\Doctrine\DoctrineDocumentReference;
use LP\PropertyBinder\Doctrine\DoctrineEntityReference;
use LP\PropertyBinder\Error\PropertyBinderError;
use LP\PropertyBinder\Handler\Binding\PropertyBindingInterface;
use LP\PropertyBinderBundle\DependencyInjection\Compiler\PropertyBindingCompilerPass;
use RuntimeException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class LPPropertyBinderExtension extends ConfigurableExtension {

    /**
     * Configures the passed container according to the merged configuration.
     *
     * @param array            $config
     * @param ContainerBuilder $container
     *
     * @throws \ReflectionException
     */
    protected function loadInternal(array $config, ContainerBuilder $container) {
        $container
            ->registerForAutoconfiguration(PropertyBindingInterface::class)
            ->addTag(PropertyBindingCompilerPass::TAG_NAME);


        $loader = new XmlFileLoader($container, new FileLocator(array(
            __DIR__ . '/../Resources/config/')));
        $loader->load('services.xml');

        $bundles = $container->getParameter('kernel.bundles');

        // metadata
        if ('none' === $config['metadata']['cache']) {
            $container->removeAlias('lp_property_binder.metadata.cache');
        } elseif ('file' === $config['metadata']['cache']) {
            $container
                ->getDefinition('lp_property_binder.metadata.cache.file_cache')
                ->replaceArgument(0, $config['metadata']['file_cache']['dir']);

            $dir = $container->getParameterBag()->resolveValue($config['metadata']['file_cache']['dir']);
            if (!is_dir($dir) && !@mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new PropertyBinderError(sprintf('Could not create cache directory "%s".', $dir));
            }
        } else {
            $container->setAlias('lp_property_binder.metadata.cache', new Alias($config['metadata']['cache'], false));
        }

        $container
            ->getDefinition('lp_property_binder.metadata_factory')
            ->replaceArgument(2, $config['metadata']['debug']);


        // directories
        $directories = array();
        if ($config['metadata']['auto_detection']) {
            foreach ($bundles as $name => $class) {
                $ref = new \ReflectionClass($class);

                $dir = dirname($ref->getFileName()) . '/Resources/config/propertyBinder';
                if (file_exists($dir)) {
                    $directories[$ref->getNamespaceName()] = $dir;
                }
            }
        }
        foreach ($config['metadata']['directories'] as $directory) {
            $directory['path'] = rtrim(str_replace('\\', '/', $directory['path']), '/');

            if ('@' === $directory['path'][0]) {
                $pathParts = explode('/', $directory['path']);
                $bundleName = substr($pathParts[0], 1);

                if (!isset($bundles[$bundleName])) {
                    throw new RuntimeException(sprintf('The bundle "%s" has not been registered with AppKernel. Available bundles: %s', $bundleName, implode(', ', array_keys($bundles))));
                }

                $ref = new \ReflectionClass($bundles[$bundleName]);
                $directory['path'] = dirname($ref->getFileName()) . substr($directory['path'], strlen('@' . $bundleName));
            }

            $dir = rtrim($directory['path'], '\\/');
            if (!file_exists($dir)) {
                throw new RuntimeException(sprintf('The metadata directory "%s" does not exist for the namespace "%s"', $dir, $directory['namespace_prefix']));
            }

            $directories[rtrim($directory['namespace_prefix'], '\\')] = $dir;
        }
        $container
            ->getDefinition('lp_property_binder.metadata.file_locator')
            ->replaceArgument(0, $directories);

        // Doctrine Reference
        $container->setParameter('lp_property_binder.manager', $config['doctrine']['manager']);
    }

    public function getConfiguration(array $config, ContainerBuilder $container) {
        return new Configuration($container->getParameterBag()->resolveValue('%kernel.debug%'));
    }
}