<?php
/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */
declare(strict_types=1);
namespace LP\PropertyBinderBundle\EventListener;

use Doctrine\Common\Annotations\Reader;
use LP\PropertyBinder\PropertyBinder;
use LP\PropertyBinderBundle\Annotation\BodyConverter;
use LP\PropertyBinderBundle\Exception\BodyConverterException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ControllerBodyConverterRequestListener
 * @package LP\PropertyBinderBundle\EventListener
 */
class ControllerBodyConverterRequestListener {
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var PropertyBinder
     */
    private $propertyBinder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param Reader         $reader
     * @param PropertyBinder $propertyBinder
     */
    public function __construct(Reader $reader, PropertyBinder $propertyBinder) {
        $this->reader = $reader;
        $this->propertyBinder = $propertyBinder;
    }

    /**
     * @param FilterControllerEvent $event
     *
     * @throws BodyConverterException
     * @throws \ReflectionException
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        if (!is_array($controllers = $event->getController())) {
            return;
        }

        $request = $event->getRequest();
        $content = $request->getContent();

        list($controller, $methodName) = $controllers;

        $reflectionObject = new \ReflectionObject($controller);
        $reflectionMethod = $reflectionObject->getMethod($methodName);
        /** @var BodyConverter $methodAnnotation */
        $methodAnnotation = $this->reader->getMethodAnnotation($reflectionMethod, BodyConverter::class);

        if (!($methodAnnotation)) {
            return;
        }

        // Search for the method parameter name that is given in the annotation
        $reflectionParameter = $this->getReflectionParameter($reflectionMethod, $methodAnnotation);
        $parameterType = $reflectionParameter->getType();
        if($parameterType === null) {
            throw new \InvalidArgumentException(sprintf('Cant bind data to an unknown data type for method parameter "%s". 
            The type has to be an object and the type hinting is missing. Method: "%s"',
                $methodAnnotation->value,
                $reflectionMethod->getDeclaringClass()->getName() . '::' . $reflectionMethod->name
            ));
        }

        if ($request->getContentType() !== 'json' ) {
            throw new \Exception(sprintf('To use this annotation on a controller method the request type has to be json.'));
        }

        // Bind the data
        $data = json_decode($content, true);
        $object = new $parameterType();
        $this->propertyBinder->bind($object, $data, $methodAnnotation->propertyBinderGroups);

        // Validate
        if($this->validator !== null && $methodAnnotation->validation) {
            $validationErrorList = $this->validator->validate($object, $methodAnnotation->validationGroups);
            if(count($validationErrorList) > 0) {
                throw new BodyConverterException($validationErrorList);
            }
        }

        $request->attributes->set($methodAnnotation->value, $object);
    }

    /**
     *
     * @param \ReflectionMethod $reflectionMethod
     * @param BodyConverter     $methodAnnotation
     *
     * @return \ReflectionParameter
     */
    private function getReflectionParameter(\ReflectionMethod $reflectionMethod, BodyConverter $methodAnnotation):\ReflectionParameter {
        foreach($reflectionMethod->getParameters() as $reflectionParameter) {
            if($methodAnnotation->value === $reflectionParameter->getName()) {
                return $reflectionParameter;
            }
        }
        throw new \InvalidArgumentException(sprintf('Cant find parameter name "%s" in method "%s" in class "%s"',
            $methodAnnotation->value,
            $reflectionMethod->getName()),
            $reflectionMethod->getDeclaringClass()->getName()
        );
    }

    /**
     * @param ValidatorInterface $validator
     */
    private function setValidator(ValidatorInterface $validator):void {
        $this->validator = $validator;
    }
}