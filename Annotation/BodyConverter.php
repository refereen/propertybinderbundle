<?php
/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */
declare(strict_types=1);
namespace LP\PropertyBinderBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * Class BodyConverter
 * @package LP\PropertyBinderBundle\Annotation
 */
final class BodyConverter extends Annotation {

    /**
     * groups for the property binder
     * @var array
     */
    public $propertyBinderGroups = [];

    /**
     * groups for validation
     * @var array
     */
    public $validationGroups = [];

    /**
     * Validate the Object
     * @var bool
     */
    public $validation = true;
}