<?php

/**
 * Copyright (c) 2019. Lieblingsprogrammierer rene@moellmer.net
 */

namespace LP\PropertyBinderBundle;

use LP\PropertyBinderBundle\DependencyInjection\Compiler\DoctrineCompilerPass;
use LP\PropertyBinderBundle\DependencyInjection\Compiler\PropertyBindingCompilerPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class LPPropertyBinderBundle extends Bundle {
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container) {
        $container->addCompilerPass(new PropertyBindingCompilerPass());
        $container->addCompilerPass(new DoctrineCompilerPass(), PassConfig::TYPE_OPTIMIZE);
    }

}